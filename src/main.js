import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './assets/style.scss'
import axios from 'axios'
import './assets/user.scss'
createApp(App).use(router).mount('#app')
